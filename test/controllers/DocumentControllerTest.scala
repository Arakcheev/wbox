package controllers

import models.entities.User
import org.joda.time.DateTime
import org.junit.runner.RunWith
import org.specs2.mutable._
import org.specs2.runner.JUnitRunner
import play.api.Logger
import play.api.libs.json.{JsValue, JsArray, Json}
import play.api.mvc.Cookie
import play.api.test.Helpers._
import play.api.test._

/**
 * @author Artem Arakcheev
 * @since 25.11.14
 */

@RunWith(classOf[JUnitRunner])
class DocumentControllerTest extends Specification {

  "Methods" should {
    val userUUID = "fjpcrjm7pqi1r0qqug0lg4k8f9"
    var repoId = ""
    var maskId = ""
    var releaseId = ""
    var docId = ""

    "create new repo" in new WithApplication() {
      val name = "Test new Repo"
      val request = FakeRequest("POST", "/repository/new")
        .withCookies(Cookie(User.COOKIE_EMAIL, userUUID, Some(86400)), Cookie(User.COOKIE_AUTH, userUUID))
        .withJsonBody(Json.obj(
        "name" -> name,
        "description" -> "desc"
      )).withHeaders(("Content-Type", "application/json"))
      val repoResult = controllers.RepositoryController.newRepo().apply(request)
      repoId = contentAsJson(repoResult).\("result").as[JsArray].value(0).\("data").\("uuid").as[String]
      status(repoResult) must equalTo(OK)
    }
    "get repo list" in new WithApplication() {
      val result = controllers.RepositoryController.list()(FakeRequest("GET", "/repositories")
        .withCookies(Cookie(User.COOKIE_EMAIL, userUUID, Some(86400)), Cookie(User.COOKIE_AUTH, userUUID)))
      contentAsJson(result).\("result").as[JsArray].value(0).\("data").as[JsArray].value.length must greaterThan(0)
    }
    "create new mask" in {
      val maskRequest = FakeRequest("POST", "/masks/new")
        .withCookies(Cookie(User.COOKIE_EMAIL, userUUID, Some(86400)), Cookie(User.COOKIE_AUTH, userUUID))
        .withJsonBody(Json.obj(
        "name" -> "test mask name",
        "title" -> "test mask title",
        "params" -> Json.obj(
          "page" -> "text",
          "footer" -> "text",
          "cost" -> "number"
        )
      )).withHeaders(("Content-Type", "application/json"),("X-Repository",repoId))
      val maskResult = controllers.MaskController.gen()(maskRequest)
      maskId = contentAsJson(maskResult).\("result").as[JsArray].value(0).\("data").\("id").as[String]
      status(maskResult) must equalTo(200)
    }
    "masks list" in new WithApplication() {
      val result = controllers.MaskController.list()(FakeRequest("GET", "/masks")
        .withCookies(Cookie(User.COOKIE_EMAIL, userUUID, Some(86400)), Cookie(User.COOKIE_AUTH, userUUID)).withHeaders(("X-Repository",repoId)))
      contentAsJson(result).\("result").as[JsArray].value(0).\("data").as[JsArray].value.length must greaterThan(0)
    }
    "create new doc" in new WithApplication() {
      val docRequest = FakeRequest("POST", s"/mask/$maskId/documents/new")
        .withCookies(Cookie(User.COOKIE_EMAIL, userUUID, Some(86400)), Cookie(User.COOKIE_AUTH, userUUID))
        .withJsonBody(Json.obj(
        "name" -> "test mask name",
        "title" -> "test mask title",
        "tags" -> List("tag1"),
        "params" -> Json.obj(
          "page" -> "text",
          "footer" -> "text",
          "cost" -> "number"
        )
      )).withHeaders("Content-Type" -> "text/javascript",("X-Repository",repoId))
      val docResult = controllers.DocumentController.gen(maskId)(docRequest)
      docId = contentAsJson(docResult).\("result").as[JsArray].value(0).\("data").\("uuid").as[String]
      docId must !==("")
    }
    "docs list" in new WithApplication() {
      val result = controllers.DocumentController.list(maskId)(FakeRequest("GET", "/masks")
        .withCookies(Cookie(User.COOKIE_EMAIL, userUUID, Some(86400)), Cookie(User.COOKIE_AUTH, userUUID)).withHeaders(("X-Repository",repoId)))
      contentAsJson(result).\("result").as[JsArray].value(0).\("data").as[JsArray].value.length must greaterThan(0)
    }
    "create new release" in new WithApplication() {
      val releaseRequest = FakeRequest("POST", s"/mask/$maskId/documents/releases/new")
        .withCookies(Cookie(User.COOKIE_EMAIL, userUUID, Some(86400)), Cookie(User.COOKIE_AUTH, userUUID))
        .withJsonBody(Json.obj(
        "name" -> "release 2",
        "publishDate" -> DateTime.now().getMillis,
        "unpublishDate" -> DateTime.now().plusDays(2).getMillis
      )).withHeaders(("Content-Type", "text/javascript"),("X-Repository",repoId))
      val releaseResult = controllers.ReleaseController.newRelease(maskId)(releaseRequest)
      releaseId = contentAsJson(releaseResult).\("result").as[JsArray].value(0).\("data").\("id").as[String]
      status(releaseResult) must equalTo(OK)
      releaseId must !==("")
    }
      "add doc to release" in new WithApplication() {
      val addDocToReleaseRequest = FakeRequest("POST", s"/releases/documents/add")
        .withCookies(Cookie(User.COOKIE_EMAIL, userUUID, Some(86400)), Cookie(User.COOKIE_AUTH, userUUID))
        .withJsonBody(Json.obj(
        "release" -> releaseId,
        "doc" -> docId
      )).withHeaders(("Content-Type", "text/javascript"),("X-Repository",repoId))
      val addDocToReleaseResult = controllers.ReleaseController.pushToRelease()(addDocToReleaseRequest)
      val newDocId = contentAsJson(addDocToReleaseResult).\("result").as[JsArray].value(0).\("data").\("id").as[String]
      status(addDocToReleaseResult) must equalTo(OK)
      newDocId must !==("")
    }
  }


}
